import threading
from .wikipath import get_path


class WikiWorker(threading.Thread):
    """Worker thread to get Wikipath.
    
       Attriburtes:
           path    A Wikipath object.
           origin  The root article.
           target  The specified endpoint.
    """
    def __init__(self, origin, target):
        self.path = None
        self.origin = origin
        self.target = target

        super(WikiWorker, self).__init__()

    def run(self):
        """Sets path to a finished Wikipath object."""
        self.path = get_path(self.origin, self.target)
