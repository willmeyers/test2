import sys
import threading
import requests
from .wikiworker import WikiWorker
from .wikipath import get_path


def random_url():
    """Returns a random Wikipedia article URL."""
    return requests.get('https://en.wikipedia.org/wiki/Special:Random').url


def main():
    print('Creating wikipath data file...')
    wikipath_data = open('data.txt', 'a+')
    
    print('Getting paths (this could take some time)...')
    for i in range(int(sys.argv[1])):
        origin = random_url()
        w = WikiWorker(origin, sys.argv[2])
        w.start()
        w.join()
        if w.path:
            print('%d/%d (%d%%)' % (i+1, int(sys.argv[1]), ((i+1)/int(sys.argv[1]))*100), origin, 'done.')
            wikipath_data.write(str(origin)+'|'+str(w.path.endpoint)+'|'+str(len(w.path))+'\n')
        else: continue

    wikipath_data.close()
    print('Done.')
    
          
if __name__ == '__main__':
    main()
