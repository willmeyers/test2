import re
import requests
import threading
from bs4 import BeautifulSoup


def get_path(origin, target):
    """Returns a finished Wikipath instance."""
    w = WikiPath(origin, target)
    w.crawl(origin)

    return w


class WikiPath:
    def __init__(self, origin, target):
        """Gets the path of Wikipedia article links to a specified target.

        :param origin:    A string, the root article.
        :param target:    A string, the specified endpoint.

        Attributes:
            base_url    The base Wikipedia article url.
            max_depth   The maxium crawl depth.
            origin      The root article.
            target      The specified endpoint.
            endpoint    The final article crawled.
            nodes       The list of crawled nodes.

        """
        self.base_url = 'https://en.wikipedia.org{}'
        self.max_depth = 100
        
        self.origin = origin
        self.target = target
        self.endpoint = None
        self.nodes = []

    def __len__(self):
        """Returns the length of nodes."""
        return len(self.nodes)

    def get_body_paragraphs(self, body):
        """Returns the list of body paragraphs."""
        try:
            paragraphs = filter(lambda p: p.has_attr('class') == False, body.findChildren('p', recursive=False))
        except AttributeError:
            return None
        
        return list(paragraphs)
        
    def get_next_link(self, para):
        """Returns the first valid Wikipedia article link."""
        links = para.findAll('a')
        ignored_links = []

        # Find all text within parentheses.
        for item in re.findall('\((.*?)\)', str(para)):
            for a in BeautifulSoup(item, 'html.parser').findAll('a'):
                # For every link within parentheses, add it to the ignored_links list.
                ignored_links.append(a['href'])

        # Ignore links within <sup></sup> (citations).
        ignored_links += [a['href'] for a in filter(lambda a: 'sup' in [t.name for t in a.findParents()], links)]

        # Ignore links within <i></i> (italics).
        ignored_links += [a['href'] for a in filter(lambda a: 'i' in [t.name for t in a.findParents()], links)]

        try:
            valid_links = list(filter(lambda a: a['href'] not in ignored_links, links))
        except AttributeError:
            return self.base_url.format(links[0]['href'])
                              
        return self.base_url.format(valid_links[0]['href'])
        
    def crawl(self, url):
        """"Main recursive method that crawls Wikipedia article"""
        self.nodes.append(url)
        
        resp = requests.get(url)
        soup = BeautifulSoup(resp.content, 'html.parser')
        
        body = soup.find('div', {'class': 'mw-parser-output'})
        body_paragraphs = self.get_body_paragraphs(body)

        for p in body_paragraphs:
            try:
                next_a = self.get_next_link(p)
                break
            except (TypeError, IndexError):
                next_a = None
                
        if next_a == None:
            self.endpoint = url
            return self.nodes
        
        if next_a in self.nodes:
            self.endpoint = url
            return self.nodes

        if len(self.nodes) >= self.max_depth:
            self.endpoint = url
            return self.nodes
            
        if next_a == self.target:
            self.endpoint = self.target
            return self.nodes.append(self.endpoint)

        else:
            self.crawl(next_a)
