# Wikipaths

The following program was developed in order to accomplish the 
following task and answer the following questions:

> Take any random article on Wikipedia and click on the first 
> link on the main body of the article that is **not** within 
> (parenthesis) or *italicized*; If you repeat this process 
> for each subsequent article you will often end up on the 
> Philosophy page.

### Questions

1. What percentage of pages often lead to philosophy?
2. Using the random article link (found on any Wikipedia article 
   in the left sidebar), what is the distribution of path lengths 
   for 500 pages, discarding those paths that never reach the Philosophy 
   page?
3. How can you reduce the number of HTTP requests necessary for 500 
   random starting pages?

**How it works:**
The program obtains a random Wikipedia article via Wikipedia's API. Then, gets the first valid link from the article body. It crawls, appending each valid link to its list of nodes until it encounters the specified target or finds a link that it has already visited (which would result in an infinite loop).

## Usage

The program only requires the user to specify the number of paths to collect and  a target. To run the program, do the following:

```bash
git clone https://gitlab.com/willmeyers/test2.git
cd test2
pip install -r requirements.txt
python -m wikipath 5 https://en.wikipedia.org/wiki/Philosophy
```

Where *5* is the number of paths you would like to collect and *https://en.wikipedia.org/wiki/Philosophy* is the *target*.

The program does not support specifying an origin via the command line, 
as that was not the original intention. However, it is possible by 
simply calling the function `get_path` with an origin and 
executing that by itself.

The program creates a text file (*data.txt*) with all Wikipath origins, endpoints, and path lengths. To create a CSV file from that text file, do the following:

```python
>>> import csv
>>> txt = csv.reader(open('data.txt', 'rt'), delimiter='|')
>>> out = csv.writer(open('data.csv', 'wt'))
>>> out.writerows(txt)
```

## Results and Discussion

An example dataset (*wikipath_dataset.csv*) is included in this repo, and is what's used to gather the statistics below.

### Results

You'll see that *Figure 1* shows us that the majority of articles do not actually point to the Philosophy page, but the Mathematics one instead. From the endpoints collected from the 500+ samples, Mathematics accounted for **~89%** of all random articles, with Philosophy making up **~7%**.

![Figure 1](Figure_1.png)

As you can see, Mathematics and Philosophy are the dominate endpoints. The other listed pages are articles that had no links in their body paragraphs and the rest are most likey errors. Further analysis is required.

For now, we'll focus only on articles which made it to the Philosophy or Mathematics artlce. In *Figure 2*, we can see the distribution of path lengths for both Philosophy and Mathematics.

![Figure 2](Figure_2.png)

The average path length for Philosophy comes to 9.39, which we can round down to **9**. For Mathematics it comes to 20.38, which rounded down gives **20**.

## Challenges and Optimizations

### Challenges

Due to Wikipedia's surprisingly different internal layouts for its articles, scraping valid links became a challenge after realizing all of the edge cases that exist. And so, some articles failed to return an endpoint.

It should be noted that the link *https://en.wikipedia.org/wiki/Element_(mathematics)* is one link away from the Mathematics page, but the program does not click on the article link within its body paragraph due to a unique edge case. The links were corrected after collection in the csv file.

### Optimizations

To reduce the number of HTTP requests, the Wikipath object could have access to a reference of common paths, that is a list of nodes that lead to an endpoint.

For example, the Science article has the endpoint Mathematics. It follows the path:

`Science -> Knowledge -> Fact -> Reality -> Object of the mind -> Mathematics`

And so, if a Wikipath were to come accross the Science article, it would simply append the path associated with Science. 

## Conclusions

From the collected data, it appears that Mathematics has taken Philosophy's place as the dominant endpoint. Of course, further research and analysis is required for more accurate results.